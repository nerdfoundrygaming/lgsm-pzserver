FROM gameservermanagers/linuxgsm-docker:main

LABEL maintainer="nfgCodex <nfg.codex@outlook.com>"

USER root

## Install Project Zomboid Requirements
RUN echo "**** Install Project Zomboid Requirements ****" \
    && apt-get update \
    && apt-get install -y \
        rng-tools udev \
        openjdk-16-jre-headless \

    # Install Cleanup
    && echo "**** Cleanup ****"  \
    && apt-get -y autoremove \
    && apt-get -y clean \
    && rm -rf /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/*

##Need use xterm for LinuxGSM##

ENV DEBIAN_FRONTEND noninteractive
ENV GAMESERVER=pzserver

COPY entrypoint-alt.sh /home/linuxgsm/entrypoint-alt.sh

RUN chown -R linuxgsm:linuxgsm /home/linuxgsm && chmod +x /home/linuxgsm/*.sh

USER linuxgsm

CMD [ "bash", "./entrypoint-alt.sh" ]
