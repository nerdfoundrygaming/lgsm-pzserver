# Linux Game Server Manager (LGSM) - Project Zomboid

This repo is a fork of the [*Official* LinuxGSM-Docker](https://github.com/GameServerManagers/LinuxGSM-Docker) repo, but modified specifically for rapid Project Zomboid deployment!

The main motivator was that I got tired of waiting between start-ups! It seems every implementation of a Project Zomboid server wants to reinstall the entire world on startup!

Since relying on LinuxGSM to be the maintainer of `steamcmd`/`pzserver` bootstrapping, this image *shouldn't* ever need an update. Heh, famous last words!

The source for this image can be found here: https://bitbucket.org/nerdfoundrygaming/lgsm-pzserver

## Usage

### docker-compose

Below is an example `docker-compose` for building locally. If you choose to use the [Docker Hub image](https://hub.docker.com/r/nerdfoundry/lgsm-pzserver/), you can just remove the *build* section.

```
version: '3.7'

services:
  lgsm-pzserver:
    image: nerdfoundry/lgsm-pzserver
    container_name: zomboid
    restart: unless-stopped
    stdin_open: true
    tty: true
    ports:
      - '8766-8767:8766-8767/udp'
      - '16261-16278:16261-16278/udp'
      - '27015:27015'
    volumes:
      # Caches home dir, including lgsm/steamcmd/etc
      - lgsm-cache:/home/linuxgsm
      # Caches Zomboid Server Install (settings, world) for easy backups
      - /docker-volumes/project-zomboid/server-files:/home/linuxgsm/Zomboid
      # LGSM Configuration
      - /docker-volumes/project-zomboid/config-lgsm:/home/linuxgsm/lgsm/config-lgsm/pzserver

volumes:
  lgsm-cache:
```

## First Run

On first run linuxgsm will install your selected server and will start running.
Once completed the game server details will be output.

## Logs

Because of the way the upstream Docker image is set to startup in the `entrypoint.sh`, Zomboid logs won't be accessible via `docker log` until subsequent reboots!

> You can still view the logs as originally intended via the upstream LGSM image, via `/home/linuxgsm/log/console/pzserver.console.log` until you restart the container.

## Volumes

> **Volumes are required** to save persistant data for your game server. If you do not properly use them, your game server will reset every container boot!

The example above includes a `lgsm-cache` volume to enable all-encompass caching (and re-usable for all LGSM-based containers).

However, for easier access, a sub-volume may be used for the actual Zomboid server files (also included in example).

Optionally, you can easily access/re-use the LGSM config files with a volume mount as well, shown in the example.

## Ports

The ports defined allow for the initial default ports. If you change them in any LGSM/Zomboid configs, of course you'll need to forward them here as well.

Out of the box, supports 16 Players (`16262-16278`)

## Run LinuxGSM commands

Commands can be run just like standard LinuxGSM using the `docker exec` command.

```
docker exec -it zomboid ./pzserver details
```

## Building Locally

Building locally is easy, although likely unnecessary:

```
docker-compose build
```

## Updating / Troubleshooting

I've noticed the way LGSM and their docker-env is configured to be deployed, it doesn't lend itself well to volume management without manually volume mounting EVERY necessary folder, or starting to rewrite the core of their bootstrapping. I didn't want to deviate too far from upstream, and the goal is to minimize maintenance.

For the time being the best solution for both Updating and Troubleshooting weird issues is to delete the cache volume, and restart the container:

```
docker rm -f zomboid
docker volume ls            # take note of the volume name you want to remove
docker volume rm lgsm-cache # use the volume name identified just before
docker-compose up
```

> Note: You'll need to find the volume name, as the project namespace (aka folder name) may differ from system to system.

This will most notably be useful when LGSM updates the `entrypoint.sh`, or should they adjust the `WORKDIR` file layout this should just clean it up.